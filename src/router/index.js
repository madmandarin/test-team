import Vue from 'vue';
import VueRouter from 'vue-router';
import User from '../views/User.vue';
import Team from '../views/Team.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Profile',
    component: User,
    meta: {
      title: 'Мой профиль',
    },
  },
  {
    path: '/team',
    name: 'Team',
    component: Team,
    meta: {
      title: 'Моя команда',
    },
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
