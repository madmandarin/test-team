import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

import getters from './getters';
import actions from './actions';
import mutations from './mutations';

Vue.use(Vuex);
const state = {
  user: {
    url: undefined,
    login: undefined,
    createDate: undefined,
    avatarURL: undefined,
    repos: [
      // {
      //   name,
      //   description,
      //   lang,
      //   cloneURL,
      //   createDate,
      // },
    ],
    followings: [
      // {
      //   login,
      //   url,
      //   avatarURL,
      // },
    ],
  },
  users: [
    // {
    //   id,
    //   login,
    //   url,
    //   avatarURL,
    //   inTeam,
    // },
  ],
};

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  plugins: process.env.NODE_ENV !== 'production'
    ? [createLogger()]
    : [],
});
