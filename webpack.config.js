module.exports = {
  rules: [
    {
      test: /\.s(c|a)ss$/,
      use: [
        'vue-style-loader',
        'css-loader',
        {
          loader: 'sass-loader',
          // Requires sass-loader@^8.0.0
          options: {
            // eslint-disable-next-line global-require
            implementation: require('sass'),
            sassOptions: {
              // eslint-disable-next-line global-require
              fiber: require('fibers'),
              indentedSyntax: true, // optional
            },
          },
        },
      ],
    },
  ],
}
