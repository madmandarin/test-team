import axios from 'axios';
import config from '../config';

const AXIOS = axios.create({
  headers: { Authorization: `token ${config.gitHubToken}` },
});

export function getProfile() {
  return AXIOS.get(`https://api.github.com/users/${config.username}`);
}
export function getRepos(page) {
  return AXIOS.get(`https://api.github.com/users/${config.username}/repos?page=${page}&per_page=100`);
}
export function getFollowings(page) {
  return AXIOS.get(`https://api.github.com/users/${config.username}/following?page=${page}&per_page=100`);
}
export function getUsers() {
  return AXIOS.get('https://api.github.com/users?since=50000000');
}
export function getRepLangs(repo) {
  return AXIOS.get(`https://api.github.com/repos/${config.username}/${repo}/languages`);
}
