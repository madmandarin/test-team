import {
  GET_MAIN_USER,
  GET_USERS,
  GET_REP_LANG,
  CHANGE_USER_TEAM_FROM_ID,
} from './action-types';
import {
  getProfile, getRepos, getFollowings, getUsers, getRepLangs,
} from '../api';
import {
  SET_MAIN_USER,
  PUSH_MAIN_USER_REPOS,
  PUSH_MAIN_USER_FOLLOWS,
  SET_USERS,
  SET_REP_LANG,
  CHANGE_USER_TEAM,
} from './mutation-types';


const recursGetRepos = async (commit, state, dispatch, page) => {
  try {
    const resRepos = await getRepos(page);
    commit(PUSH_MAIN_USER_REPOS, resRepos.data.map((rep) => ({
      name: rep.name,
      description: rep.description,
      lang: rep.language ? rep.language : 'Loading...',
      cloneURL: rep.clone_url,
      url: rep.html_url,
      createDate: rep.created_at,
    })));
    if (resRepos.data.length === 100) {
      recursGetRepos(commit, state, dispatch, page + 1);
    } else {
      state.user.repos.forEach((rep, index) => {
        if (rep.lang === 'Loading...') {
          dispatch(GET_REP_LANG, index);
        }
      });
    }
  } catch (err) {
    console.log(err);
  }
};
const recursGetFollows = async (commit, page) => {
  try {
    const resFollows = await getFollowings(page);
    commit(PUSH_MAIN_USER_FOLLOWS, resFollows.data.map((user) => ({
      login: user.login,
      url: user.html_url,
      avatarURL: user.gravatar_id === '' ? user.avatar_url : `https://www.gravatar.com/avatar/${user.gravatar_id}`,
    })));
    if (resFollows.data.length === 100) {
      recursGetFollows(commit, page + 1);
    }
  } catch (err) {
    console.log(err);
  }
};
export default {
  async [GET_MAIN_USER]({ commit, state, dispatch }) {
    try {
      const resUser = await getProfile();
      commit(SET_MAIN_USER, {
        url: resUser.data.html_url,
        login: resUser.data.login,
        createDate: resUser.data.created_at,
        avatarURL: resUser.data.gravatar_id === '' ? resUser.data.avatar_url : `https://www.gravatar.com/avatar/${resUser.data.gravatar_id}`,
        repos: [],
        followings: [],
      });
      await recursGetRepos(commit, state, dispatch, 1);
      await recursGetFollows(commit, 1);
    } catch (err) {
      console.log(err);
    }
  },
  [GET_USERS]({ commit }) {
    getUsers().then((resUsers) => {
      commit(SET_USERS, resUsers.data.map((user) => ({
        id: user.id,
        login: user.login,
        url: user.html_url,
        avatarURL: user.gravatar_id === '' ? user.avatar_url : `https://www.gravatar.com/avatar/${user.gravatar_id}`,
        inTeam: false,
      })));
    }).catch((err) => console.log(err));
  },
  async [GET_REP_LANG]({ state, commit }, data) {
    try {
      const resLang = await getRepLangs(state.user.repos[data].name);
      let langs = '';
      // eslint-disable-next-line guard-for-in,no-restricted-syntax
      for (const lang in resLang.data) {
        langs = `${langs} ${lang}`;
      }
      if (langs === '') {
        langs = 'Others';
      }
      commit(SET_REP_LANG, { id: data, langs });
    } catch (err) {
      console.log(err);
    }
  },
  [CHANGE_USER_TEAM_FROM_ID]({ state, commit }, id) {
    state.users.forEach((user, index) => {
      if (user.id === id) {
        commit(CHANGE_USER_TEAM, index);
      }
    });
  },
};
