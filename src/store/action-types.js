export const GET_MAIN_USER = 'GET_MAIN_USER';
export const GET_USERS = 'GET_USERS';
export const GET_REP_LANG = 'GET_REP_LANG';
export const CHANGE_USER_TEAM_FROM_ID = 'CHANGE_USER_TEAM_FROM_ID';
