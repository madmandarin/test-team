import {
  GET_TEAM,
  GET_NON_TEAM_USERS,
} from './getter-types';

export default {
  [GET_TEAM]: (state) => (order) => state.users.filter((user) => user.inTeam === true)
    .sort((a, b) => {
      const login1 = a.login.toLowerCase();
      const login2 = b.login.toLowerCase();
      if (login1 === login2) {
        return 0;
      }
      if ((login1 > login2) === order) {
        return 1;
      }
      return -1;
    }),
  [GET_NON_TEAM_USERS]: (state) => (search) => state.users.filter(
    (user) => (
      (user.inTeam === false) && (user.login.toLowerCase().indexOf(search.toLowerCase()) !== -1)
    ),
  ),
};
