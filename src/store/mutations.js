import Vue from 'vue';
import {
  SET_MAIN_USER,
  PUSH_MAIN_USER_REPOS,
  PUSH_MAIN_USER_FOLLOWS,
  SET_USERS,
  SET_REP_LANG,
  CHANGE_USER_TEAM,
} from './mutation-types';

export default {
  [SET_MAIN_USER](state, data) {
    state.user = {
      ...data,
      repos: undefined,
      followings: undefined,
    };
  },
  [PUSH_MAIN_USER_REPOS](state, data) {
    if (state.user.repos && data.length) {
      state.user.repos.push(...data);
    } else {
      state.user.repos = data;
    }
  },
  [PUSH_MAIN_USER_FOLLOWS](state, data) {
    if (state.user.followings) {
      state.user.followings.push(...data);
    } else {
      state.user.followings = data;
    }
  },
  [SET_USERS](state, data) {
    state.users = data;
  },
  [SET_REP_LANG](state, { id, langs }) {
    Vue.set(state.user.repos[id], 'lang', langs);
  },
  [CHANGE_USER_TEAM](state, id) {
    Vue.set(state.users[id], 'inTeam', !state.users[id].inTeam);
  },
};
